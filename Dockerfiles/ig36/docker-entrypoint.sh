#!/bin/bash


/opt/netiq/idm/apps/tomcat/bin/startup.sh

if [ $? != 0 ]
then
		echo "%%% Tomcat no pudo iniciar."
		exit 1
fi

cd /opt/netiq/idm/apps/tomcat/logs/

tail -f $(ls -t /opt/netiq/idm/apps/tomcat/logs/ | grep .log | head -n 1)
